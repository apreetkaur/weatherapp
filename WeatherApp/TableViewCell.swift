//
//  TableViewCell.swift
//  WeatherApp
//
//  Created by Neeraj on 20/1/22.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var pressure: UILabel!
    
    @IBOutlet weak var humidity: UILabel!
    
    @IBOutlet weak var windspeed: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
