//
//  Tableview.swift
//  WeatherApp
//
//  Created by Neeraj on 20/1/22.
//

import UIKit

class Tableview: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var tableview: UITableView!
    var hourly : [Hourly] = []
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableview.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tableview.delegate = self
        tableview.dataSource = self
        // print("array count \(array.count)")

        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hourly.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.pressure.text = "Pressure =\(hourly[indexPath.row].pressure)"
        cell.humidity.text = "Humidity =\(hourly[indexPath.row].humidity)"
        cell.windspeed.text = "WindSpeed = \(hourly[indexPath.row].wind_speed)"
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0{
//            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TableViewCell") as? TableViewCell
//            vc?.labelText = "Pressure"
//            self.navigationController?.pushViewController(vc!, animated: true)
//        }else if indexPath.row == 1{
//            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TableViewCell") as? TableViewCell
//            vc?.labelText = "Humidity"
//            self.navigationController?.pushViewController(vc!, animated: true)
//            if indexPath.row == 2{
//                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TableViewCell") as? TableViewCell
//                vc?.labelText = "Windspeed"
//                self.navigationController?.pushViewController(vc!, animated: true)
//            }
//        }
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
