//
//  WeatherViewController.swift
//  WeatherApp
//
//  Created by Neeraj on 11/1/22.
//

import UIKit

class WeatherViewController: UIViewController

{
    
    
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var sunsetLabel : UILabel!
    
    @IBOutlet weak var sunriseLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var hourly : [Hourly] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sessionCreation()
        // Do any additional setup after loading the view.
    }
    
    func sessionCreation(){
        self.activityIndicator.startAnimating()
        var request = URLRequest(url: URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=27.4705&lon=153.0260&appid=f114208015a77711f92ed05765670629")!)
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            }
            
            print(response)
            do {
                if let data = data,
                   let weatherObj = try? JSONDecoder().decode(WeatherModel.self, from: data) {
                    DispatchQueue.main.async {
                        self.sunsetLabel.text = "Sunset =" + "\(weatherObj.current?.sunset ?? 0)"
                        self.sunriseLabel.text = "Sunrise =" +  "\(weatherObj.current?.sunrise ?? 0)"
                        self.temperatureLabel.text = "Temperature =" + "\(weatherObj.current?.temp ?? 0)"
                        self.hourly = weatherObj.hourly ?? []
                    }
                }
                
            } catch {
                print("error")
            }
        })
        task.resume()
        
    }
    
    @IBAction func btnHourly(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Tableview") as? Tableview
        vc?.hourly = self.hourly
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
}
struct WeatherModel : Decodable{
    var current : Current?
    var hourly : [Hourly]?
}

struct Current : Decodable{
    var sunrise : Int
    var sunset : Int
    var temp : Double
}

struct Hourly : Decodable{
    var pressure : Int
    var humidity : Int
    var wind_speed : Double
    var temp : Double
}
